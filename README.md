# Getting Started

## Clone The Repo
`git clone git@gitlab.com:petertoth.sandbox/laravel-todo-ct.git`

## .env
Create a `.env` file with your credentials

## Install Packages
- `composer install`

## Setup and optimize
Move to your working directory, then run these commands

- `composer dump-autoload`
- `chown -R {user}:{group} .`
- `chmod 777 -R ./storage/`
- `php artisan optimize`
- `php artisan migrate`
- `php artisan db:seed --class=ProjectSeeder`

# The task
Create a very simple Laravel web application for task management:

- Create task (info to save: task name, priority, timestamps)
- Edit task
- Delete task
- Reorder tasks with drag and drop in the browser. Priority should automatically be updated based on this. #1 priority goes at top, #2 next down and so on.
- Tasks should be saved to a mysql table.
- **BONUS POINT**: add project functionality to the tasks. User should be able to select a project from a dropdown and only view tasks associated with that project.

### Comment
Since the task was creating a *simple* application the most *Laravel* way, I used blade engine instead of a frontend framework and/or XHR requests. 
To show an XHR example besides blade, I implemented the `Delete` function using `XHR` and the api route.


