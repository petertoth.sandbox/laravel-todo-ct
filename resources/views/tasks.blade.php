@extends('layout')

@section('content')
    <div class="container">
        <!-- Project Selector -->
        <div class="card my-4">
            <div class="card-body">
                <h2>Your Projects</h2>
                <select name="project_id" id="project_selector" class="form-select">
                    @foreach ($projects as $project)
                        <option value="{{$project->id}}" {{ ( (int) $project->id === (int) Request::route('project')->id ) ? 'selected' : '' }}>{{$project->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <!-- Create Task Form -->
        <form action="{{ route('tasks.store', ['project' => Request::route('project')->id]) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <!-- Task Name -->
            <div class="form-group">
                <label for="name" class="control-label"><h2>Create a new Task</h2></label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Task name" autofocus>
            </div>

            <!-- Submit -->
            <div class="form-group mt-3">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-plus"></i> Add Task
                </button>
            </div>
        </form>

        <!-- Tasks -->
        <h2 class="mt-5 mb-3">Your Tasks</h2>
        @if($tasks->isEmpty()) <span>You don't have any tasks</span> @endif
        <div id="task-list">
            @foreach ($tasks as $task)
                <div id="task-{{ $task->id }}" class="task-row" data-task-id="{{$task->id}}">
                    <form action="{{ route('tasks.update', ['project' => Request::route('project')->id, 'task' => $task->id]) }}" method="POST" class="form-horizontal mb-2">
                        @method('PUT')
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-auto p-0"><i class="fa fa-sort" aria-hidden="true"></i></div>
                            <div class="col">
                                <input type="text" name="name" id="name" class="form-control form-control-sm" placeholder="Task name" value="{{$task->name}}">
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-floppy"></i> Save Task
                                </button>
                            </div>
                            <div class="col-auto">
                                <button type="button" class="delete-btn btn btn-sm btn-danger" data-task-id="{{$task->id}}">Delete</button>
                            </div>

                        </div>
                    </form>
                </div>
            @endforeach
        </div>
    </div>
@endsection
@push('scripts')

    <script>
        $(function() {
            // JS Handler for task deleting
            $('.delete-btn').on('click', function(){
                const $button = $(this);
                const id = $button.data('task-id');
                $.ajax({
                    url: "/api/tasks/"+id,
                    method: 'delete'
                })
                .done(function( data ) {
                    $button.parents('.task-row').remove();
                    $( "#task-list" ).sortable( "refresh" );
                });
            })

            // JS Handler for task reordering
            $( "#task-list" ).sortable({
                update: function( event, ui ) {
                    const order = $(this).sortable('toArray', { attribute: 'data-task-id' });
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('tasks.reorder') }}',
                        data: {
                            order: order,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });
                }
            });

            // JS Handler for project selecting
            document.querySelector('#project_selector').addEventListener('change', function() {
                window.location.href = '{{ route("tasks.index", ['project' =>':projectId']) }}'.replace(':projectId', this.value);
            });
        });
    </script>
@endpush
