<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display each Task
     */
    public function index(Request $request, Project $project)
    {
        $projects = Project::orderBy('title', 'asc')->get();
        $tasks = Task::query()->orderBy('priority', 'asc')->where('project_id',$project->id)->get();

        return view('tasks', [
            'tasks'         => $tasks,
            'projects'      => $projects,
        ]);
    }

    /**
     * Create a new Task
     *
     * @param Request $request
     */
    public function store(Request $request, Project $project)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $task = new Task;
        $task->name = $request->name;
        $task->project_id = $project->id;
        $task->priority = Task::where('project_id', $project->id)->count() + 1;
        $task->save();

        return redirect()->route('tasks.index', ['project' => $project->id]);
    }

    /**
     * Update the Task
     *
     * @param Request $request
     * @param  \App\Models\Task  $task
     */
    public function update(Request $request,Project $project, Task $task)
    {
        $task->name = $request->name;
        $task->save();
        return redirect()->route('tasks.index', ['project' => $project->id]);
    }

    /**
     * Delete the Task
     *
     * @param  \App\Models\Task  $task
     */
    public function destroy(Task $task)
    {
        $project_id = $task->project_id;
        $task->delete();
        Task::where('priority', '>',$task->priority)->where('project_id', $project_id)->decrement('priority');

        return response()->json([
            'message'   => 'Task has been removed'
        ]);
    }

    /**
     * Update the priority of the tasks
     *
     * @param  \App\Models\Task  $task
     */
    public function reorder(Request $request)
    {
        $order = $request->input('order');

        foreach($order as $index => $id) {
            Task::find($id)->update(['priority' => $index+1]);
        }

        return response()->json([
            'message'   => 'OK'
        ]);
    }
}
